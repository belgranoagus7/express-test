const userService = require("../services/user.service")

const getAll = (req, res) => {
    res.send(userService.getAll());
}

const getOne = (req, res) => {
    console.log(req.params)
    const user = userService.getOne(req.params.id);
    if (!user){
        res.sendStatus(404);
    }
    res.send(user);
}

const create = (req, res) => {
    const user = userService.create(req.body);
    res.send(user);
}


module.exports = {
    getAll,
    getOne,
    create
}