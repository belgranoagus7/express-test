const users = [];
let nextId = 0;

const getAll = (filter) => {
    return users;
}

const getOne = (id) => {
    return users.find(x => x.id == id)
}

const create = (payload) => {
    nextId++;
    const {name, surname} = payload;
    const user = {
        id: nextId,
        name,
        surname
    }
    users.push(user)
    return user;
}


module.exports = {
    getAll,
    getOne,
    create
}