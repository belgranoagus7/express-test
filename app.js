const express = require('express');
const app = express();
const port = 3000;
const routes = require('./routes');
const cors = require('cors');

app.use(express.json());
// form data
app.use(express.urlencoded());

app.use(cors())

// ruteo de controllers
app.use('/api', routes);


// not found
app.use((req,res) => {
    res.sendStatus(404);
})
// error handler
app.use((err, req, res, next) => {
    res.status(500).send(err.message)
})


app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})